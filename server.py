#!/usr/bin/env python3.6

from gatekeeper.factory import create_app

api = create_app()

if __name__ == '__main__':
    api.debug = True
    api.run('localhost', 7000)
