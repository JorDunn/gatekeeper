[![Codacy Badge](https://api.codacy.com/project/badge/Grade/388f256f3fca44bca1ae701ca5665263)](https://www.codacy.com/app/JorDunn/gatekeeper?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=JorDunn/gatekeeper&amp;utm_campaign=Badge_Grade)
[![Build Status](https://travis-ci.org/JorDunn/gatekeeper.svg?branch=master)](https://travis-ci.org/JorDunn/gatekeeper)

# gatekeeper
Gatekeeper is a login server that can be used for just about anything. I'm hoping to make some web games that can make use of this. I will also be adding an admin client soon.

You will also need to generate an API key using gen_apikey.py and enter it into the database after running gatekeeper once. Eventually there will be a backend for API key management.

# Configuration
Make sure to rename example_config.py to config.py and enter the relevant information.

# Running gatekeeper
    gunicorn -w <num workers> server:api

# TODO
    - use headers for authentication
    - use graphql instead of pony

# Tools
## Pony
I am using Pony ORM for this project initially because I am more familiar with it versus SQLAlchemy. Eventually I want to move to GraphQL to gain some experience with it before I use it on bigger projects.

## Flask & Flask Restful
I chose to use Flask because it allows me to rapidly develop webapps and doesn't tie me down with boilerplate or opinions like what modules to use. `flask_restful` is being used to try out a different way of making api endpoints instead of using `@app.route`.

## Javascript Web Tokens
JWTs are used to validate the users session. It is passed to the `/users/validate` endpoint to check if it is still valid. If it isnt, gatekeeper sends back `{'error': 'Invalid session'}`. If it is, `{'success': 'Valid session'}` is sent.