class Config():
    DEBUG = True
    DB_HOST = 'localhost'
    DB_USER = 'gatekeeper'
    DB_PASS = 'changeme'
    DB_DATABASE = 'gatekeeper'
    JWT_SECRET = 'changeme'
    AUTHORIZED_IPS = ['127.0.0.1']
