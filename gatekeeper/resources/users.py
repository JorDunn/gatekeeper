from datetime import datetime, timedelta

from flask import jsonify, request
from flask_restful import Resource
from jose import jwt
from nacl.pwhash import scrypt, InvalidkeyError
from pony.orm import db_session, commit, TransactionIntegrityError

from gatekeeper.models import ApiAuth, User
from gatekeeper.config import Config


def verify_headers(headers):
    if headers['Content-Type'] == 'application/json' and headers['Authorization']:
        return True
    else:
        return False


def verify_address(remote_addr):
    if remote_addr in Config.AUTHORIZED_IPS:
        return True
    else:
        return False


class Users(Resource):

    def get(self):
        return jsonify({'status_code': 405, 'message': 'method now allowed'})

    @db_session
    def post(self):
        try:
            if verify_address(request.remote_addr) is False:
                return jsonify({'status_code': 403, 'message': 'forbidden: invalid headers'})
            if verify_headers(request.headers):
                api_token = request.headers.get('Authorization')
            else:
                return jsonify({'status_code': 403, 'message': 'forbidden: invalid api token'})
            if ApiAuth.exists(token=api_token):
                api_user = ApiAuth.get(token=api_token)
                data = request.get_json()
            else:
                return jsonify({'status_code': 403, 'message': 'forbidden: invalid api token'})
            if User.exists(username=data['username']):
                user = User.get(username=data['username'])
            else:
                return jsonify({'status_code': 401, 'message': 'unauthorized: invalid username or password'})
            if scrypt.verify(user.password, data['password'].encode()):
                current_time = datetime.utcnow()
                payload = {'exp': (current_time + timedelta(hours=1)),
                           'nbf': current_time,
                           'iss': 'gatekeeper',
                           'aud': api_user.name,
                           'iat': current_time
                           }
                jwt_token = jwt.encode(payload, Config.JWT_SECRET, algorithm='HS512')
                return jsonify({'status_code': 200, 'token': f'{jwt_token}'})
            else:
                return jsonify({'status_code': 401, 'message': 'unauthorized: invalid username or password'})
        except InvalidkeyError as e:
            print(e)
            return jsonify({'status_code': 401, 'message': 'unauthorized: invalid username or password'})
        except KeyError as e:
            print(e)
            return jsonify({'status_code': 401, 'message': 'unauthorized: invalid username or password'})

    @db_session
    def put(self):
        """Used for new user creation. Does not return an auth token after user is created."""
        try:
            if request.headers.get('Content-Type') != 'application/json':
                return jsonify({'error': 'content-type must be application/json'})
            if request.headers.get('Authorization'):
                token = request.headers.get('Authorization')
            else:
                return jsonify({'status_code': 401, 'message': 'invalid api token'})
            if ApiAuth.exists(token=token):
                data = request.get_json()
                username = data['username']
                password = data['password']
                email = data['email']
                try:
                    User(username=username, password=scrypt.str(password.encode()), email=email)
                    commit()
                    return jsonify({'status_code': 200, 'message': 'user created successfully'})
                except TransactionIntegrityError as e:
                    print(e)
                    return jsonify({'status_code': 400, 'message': f'{e}'})
                except Exception as e:
                    print(e)
                    return jsonify({'status_code': 500, 'message': 'something went wrong during user creation'})
            else:
                return jsonify({'status_code': 401, 'message': 'invalid api token'})
        except InvalidkeyError as e:
            print(e)
            return jsonify({'status_code': 401, 'message': 'unauthorized: invalid username or password'})
        except KeyError:
            print(e)
            return jsonify({'status_code': 401, 'message': 'unauthorized: invalid username or password'})
