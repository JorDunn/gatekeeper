import time

from flask import jsonify, request
from flask_restful import Resource
from jose import jwt
from pony.orm import db_session

from gatekeeper.models import ApiAuth
from gatekeeper.config import Config


class UsersVerify(Resource):

    def get(self):
        return jsonify({'status_code': 405, 'message': 'method not allowed'})

    @db_session
    def post(self):
        if request.headers.get('Content-Type') != 'application/json':
            return jsonify({'error': 'content-type must be application/json'})
        if request.headers.get('Authorization'):
            token = request.headers.get('Authorization')
        else:
            return jsonify({'status_code': 401, 'error': 'invalid API token'})
        if ApiAuth.exists(token=token):
            api_user = ApiAuth.get(token=token)
            current_time = time.time()
            data = request.get_json()
            payload = jwt.decode(data['token'], Config.JWT_SECRET, algorithms='HS512',
                                 issuer='gatekeeper', audience=api_user.name)
            if payload['exp'] > current_time and payload['nbf'] < current_time and payload['iat'] < current_time:
                return jsonify({'status_code': 200, 'message': 'valid token'})
            else:
                return jsonify({'status_code': 401, 'message': 'invalid token'})
        else:
            return jsonify({'status_code': 403, 'message': 'forbidden: invalid api token'})
