from flask import Flask
from flask_restful import Api
from gatekeeper.extensions import db
from gatekeeper.resources import Users, UsersVerify
from gatekeeper.config import Config


def create_app():
    app = Flask(__name__)

    api = Api(app)
    api.add_resource(Users, '/users')
    api.add_resource(UsersVerify, '/users/verify')

    db.bind(provider='mysql', user=Config.DB_USER, password=Config.DB_PASS, db=Config.DB_NAME)
    db.generate_mapping(create_tables=True)

    return app
