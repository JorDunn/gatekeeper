from gatekeeper.extensions import db
from pony.orm import PrimaryKey, Required


class User(db.Entity):

    _table_ = 'users'

    id = PrimaryKey(int, auto=True)
    username = Required(str, unique=True)
    password = Required(bytes)
    email = Required(str, unique=True)

    def __repr__(self):
        return f'<User {self.username}>'
