from gatekeeper.extensions import db
from pony.orm import PrimaryKey, Required


class ApiAuth(db.Entity):

    _table_ = 'apiauth'

    id = PrimaryKey(int, auto=True)
    token = Required(str, unique=True)
    name = Required(str)

    def __repr_(self):
        return f'<ApiAuth {self.name}>'
